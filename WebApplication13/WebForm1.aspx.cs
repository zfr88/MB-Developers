﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using RestSharp;
using Newtonsoft.Json;
using WebApplication13.DataServices;

namespace WebApplication13
{
    public partial class WebForm1 : Page
    {
        //public string redirectUrl = "http://localhost:65206/WebForm1.aspx";
        public string redirectUrl = "http://localhost:8080/WebForm1.aspx";
        public String apiKey = "ea043cf8-9a68-4ab8-8f2a-8c4601ddd71a";
        public String apiSecret = "deb38335-f8d8-4f8f-954b-af4398e326d8";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                try
                {
                    VehicleInformation getVehicleInformation = new VehicleInformation();
                    TireStates getTireStates = new TireStates();
                    Location getLocation = new Location();
                    Odometer getOdometer = new Odometer();
                    FuelState getFuelState = new FuelState();
                    StateOfCharge getStateOfCharge = new StateOfCharge();
                    StateOfDoors getStateOfDoors = new StateOfDoors();

                    if (Request.QueryString["code"] != null)
                    {
                        string Access_token = VerifyAuthentication(Request.QueryString["code"]);
                        string VehicleId = GetVehicleId(Access_token);
                        getVehicleInformation = GetVehicleInformationById(VehicleId, Access_token);
                        getTireStates = GetTireState(VehicleId, Access_token);
                        getLocation = GetLocation(VehicleId, Access_token);
                        getOdometer = GetOdometer(VehicleId, Access_token);
                        getFuelState = GetFuelState(VehicleId, Access_token);
                        getStateOfCharge = GetStateOfCharge(VehicleId, Access_token);
                        getStateOfDoors = GetStateOfDoors(VehicleId, Access_token);
                        string kapidurum = SetLockStateOfDoors(VehicleId, Access_token);
                        if (kapidurum == "INITIATED")
                        {

                        }
                        else
                        {

                        }

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://api.secure.mercedes-benz.com/oidc10/auth/oauth/v2/authorize?response_type=code&client_id=" + apiKey + "&redirect_uri=" + HttpUtility.HtmlEncode(redirectUrl) + "&scope=mb:vehicle:status:general mb:user:pool:reader");
        }

        private string VerifyAuthentication(string code)
        {
            string authUrl = "https://api.secure.mercedes-benz.com/oidc10/auth/oauth/v2/token";

            var sign = "grant_type=authorization_code&code=" + HttpUtility.UrlEncode(code) + "&redirect_uri=" + HttpUtility.HtmlEncode(redirectUrl) + "&client_id=" + apiKey + "&client_secret=" + apiSecret;

            HttpWebRequest webRequest = WebRequest.Create(authUrl + "?" + sign) as HttpWebRequest;
            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";
            Stream dataStream = webRequest.GetRequestStream();

            String postData = String.Empty;
            byte[] postArray = Encoding.ASCII.GetBytes(postData);

            dataStream.Write(postArray, 0, postArray.Length);
            dataStream.Close();

            WebResponse response = webRequest.GetResponse();
            dataStream = response.GetResponseStream();


            StreamReader responseReader = new StreamReader(dataStream);
            String returnVal = responseReader.ReadToEnd().ToString();
            responseReader.Close();
            dataStream.Close();
            response.Close();
            var JSONObj = new JavaScriptSerializer().Deserialize<Dictionary<string, string>>(returnVal);

            return JSONObj["access_token"].ToString();
        }

        public string GetVehicleId(string access_token)
        {
            var client = new RestClient("https://api.mercedes-benz.com/experimental/connectedvehicle/v1/vehicles");
            var request = new RestRequest(Method.GET);
            //request.AddHeader("cache-control", "no-cache");
            //request.AddHeader("Connection", "keep-alive");
            //request.AddHeader("accept-encoding", "gzip, deflate");
            //request.AddHeader("Host", "api.mercedes-benz.com");
            //request.AddHeader("Postman-Token", "a2c9fb56-2d0d-4057-a4de-e6dd6a8fa66f,9e604316-58d7-4948-a78e-9c6a85bdac6a");
            //request.AddHeader("Cache-Control", "no-cache");
            //request.AddHeader("User-Agent", "PostmanRuntime/7.11.0");            
            request.AddHeader("Authorization", "Bearer " + access_token);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            IRestResponse response = client.Execute(request);

            List<Vehicles> objVehicle = JsonConvert.DeserializeObject<List<Vehicles>>(response.Content);
            return objVehicle[0].id;
        }

        public VehicleInformation GetVehicleInformationById(string VehicleId, string access_token)
        {
            var client = new RestClient("https://api.mercedes-benz.com/experimental/connectedvehicle/v1/vehicles/" + VehicleId);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + access_token);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            IRestResponse response = client.Execute(request);

            VehicleInformation JSONObj = new JavaScriptSerializer().Deserialize<VehicleInformation>(response.Content);
            return JSONObj;
        }

        public TireStates GetTireState(string VehicleId, string access_token)
        {
            var client = new RestClient("https://api.mercedes-benz.com/experimental/connectedvehicle/v1/vehicles/" + VehicleId + "/tires");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + access_token);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            IRestResponse response = client.Execute(request);

            TireStates JSONObj = JsonConvert.DeserializeObject<TireStates>(response.Content);
            return JSONObj;
        }

        public Location GetLocation(string VehicleId, string access_token)
        {
            var client = new RestClient("https://api.mercedes-benz.com/experimental/connectedvehicle/v1/vehicles/" + VehicleId + "/location");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + access_token);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            IRestResponse response = client.Execute(request);

            Location JSONObj = JsonConvert.DeserializeObject<Location>(response.Content);
            return JSONObj;
        }

        public Odometer GetOdometer(string VehicleId, string access_token)
        {
            var client = new RestClient("https://api.mercedes-benz.com/experimental/connectedvehicle/v1/vehicles/" + VehicleId + "/odometer");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + access_token);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            IRestResponse response = client.Execute(request);

            Odometer JSONObj = JsonConvert.DeserializeObject<Odometer>(response.Content);
            return JSONObj;
        }

        public FuelState GetFuelState(string VehicleId, string access_token)
        {
            var client = new RestClient("https://api.mercedes-benz.com/experimental/connectedvehicle/v1/vehicles/" + VehicleId + "/fuel");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + access_token);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            IRestResponse response = client.Execute(request);

            FuelState JSONObj = JsonConvert.DeserializeObject<FuelState>(response.Content);
            return JSONObj;
        }

        public StateOfCharge GetStateOfCharge(string VehicleId, string access_token)
        {
            var client = new RestClient("https://api.mercedes-benz.com/experimental/connectedvehicle/v1/vehicles/" + VehicleId + "/stateofcharge");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + access_token);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            IRestResponse response = client.Execute(request);

            StateOfCharge JSONObj = JsonConvert.DeserializeObject<StateOfCharge>(response.Content);
            return JSONObj;
        }

        public StateOfDoors GetStateOfDoors(string VehicleId, string access_token)
        {
            var client = new RestClient("https://api.mercedes-benz.com/experimental/connectedvehicle/v1/vehicles/" + VehicleId + "/doors");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + access_token);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            IRestResponse response = client.Execute(request);

            StateOfDoors JSONObj = JsonConvert.DeserializeObject<StateOfDoors>(response.Content);
            return JSONObj;
        }

        public string SetLockStateOfDoors(string VehicleId, string access_token)
        {
            var client = new RestClient("https://api.mercedes-benz.com/experimental/connectedvehicle/v1/vehicles/" + VehicleId + "/doors");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + access_token);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            request.AddParameter("undefined", "{\"command\" : \"UNLOCK\"}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            var JSONObj = new JavaScriptSerializer().Deserialize<Dictionary<string, string>>(response.Content);
            return JSONObj["status"].ToString();
        }
    }
}