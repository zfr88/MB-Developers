﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication13.DataServices
{
    public class StateOfCharge
    {
        [JsonProperty("stateofcharge")]
        public Attributes stateofcharge { get; set; }

        public class Attributes
        {
            public decimal value;
            public string retrievalstatus;
            public string timestamp;
            public string unit;
        }
    }
}