﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication13.DataServices
{
    public class Vehicles
    {
        public string id { get; set; }
        public string licenseplate { get; set; }
        public string finorvin { get; set; }
    }
}