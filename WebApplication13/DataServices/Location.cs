﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication13.DataServices
{
    public class Location
    {
        [JsonProperty("longitude")]
        public Attributes longitude { get; set; }

        [JsonProperty("latitude")]
        public Attributes latitude { get; set; }

        [JsonProperty("heading")]
        public Attributes heading { get; set; }
        
        public class Attributes
        {
            public string value;
            public string retrievalstatus;
            public string timestamp;
        }
    }
}