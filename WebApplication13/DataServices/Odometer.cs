﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication13.DataServices
{
    public class Odometer
    {
        [JsonProperty("odometer")]
        public Attributes odometer { get; set; }

        [JsonProperty("distancesincereset")]
        public Attributes distancesincereset { get; set; }

        [JsonProperty("distancesincestart")]
        public Attributes distancesincestart { get; set; }

        public class Attributes
        {
            public string value;
            public string retrievalstatus;
            public string timestamp;
            public string unit;
        }
    }
}