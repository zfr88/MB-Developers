﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication13.DataServices
{
    public class VehicleInformation
    {
        public string id { get; set; }
        public string licenseplate { get; set; }
        public string salesdesignation { get; set; }
        public string finorvin { get; set; }
        public string nickname { get; set; }
        public string modelyear { get; set; }
        public string colorname { get; set; }
        public string fueltype { get; set; }
        public string powerhp { get; set; }
        public string powerkw { get; set; }
        public string numberofdoors { get; set; }
        public string numberofseats { get; set; }
    }
}