﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication13.DataServices
{
    public class TireStates
    {
        [JsonProperty("tirepressurefrontleft")]
        public Attributes tirepressurefrontleft { get; set; }

        [JsonProperty("tirepressurefrontright")]
        public Attributes tirepressurefrontright { get; set; }

        [JsonProperty("tirepressurerearright")]
        public Attributes tirepressurerearright { get; set; }

        [JsonProperty("tirepressurerearleft")]
        public Attributes tirepressurerearleft { get; set; }
    }

    public class Attributes
    {
        public string value;
        public string retrievalstatus;
        public string timestamp;
    }
}