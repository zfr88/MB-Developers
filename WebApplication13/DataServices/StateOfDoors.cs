﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication13.DataServices
{
    public class StateOfDoors
    {
        [JsonProperty("doorstatusfrontleft")]
        public Attributes doorstatusfrontleft { get; set; }

        [JsonProperty("doorlockstatusfrontleft")]
        public Attributes doorlockstatusfrontleft { get; set; }

        [JsonProperty("doorstatusfrontright")]
        public Attributes doorstatusfrontright { get; set; }

        [JsonProperty("doorlockstatusfrontright")]
        public Attributes doorlockstatusfrontright { get; set; }

        [JsonProperty("doorstatusrearright")]
        public Attributes doorstatusrearright { get; set; }

        [JsonProperty("doorlockstatusrearright")]
        public Attributes doorlockstatusrearright { get; set; }

        [JsonProperty("doorstatusrearleft")]
        public Attributes doorstatusrearleft { get; set; }

        [JsonProperty("doorlockstatusrearleft")]
        public Attributes doorlockstatusrearleft { get; set; }

        [JsonProperty("doorlockstatusdecklid")]
        public Attributes doorlockstatusdecklid { get; set; }

        [JsonProperty("doorlockstatusgas")]
        public Attributes doorlockstatusgas { get; set; }

        [JsonProperty("doorlockstatusvehicle")]
        public Attributes doorlockstatusvehicle { get; set; }

        public class Attributes
        {
            public string value;
            public string retrievalstatus;
            public string timestamp;
        }
    }
}